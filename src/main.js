var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var maxSize = 0.1
var dimension = 5
var resolution = 128
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < resolution; j++) {
      push()
      translate(windowWidth * 0.5 + (j - Math.floor(resolution * 0.5)) * boardSize * 0.8 * (1 / resolution), windowHeight * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * maxSize * 1.75 + sin(frame + j * 0.1 + j * 0.1) * boardSize * maxSize * 0.25)
      fill((255 / 63) * abs(Math.floor((frame * resolution + j * 3) % 64)))
      ellipse(0, 0, boardSize * maxSize * abs(cos(frame + j * 0.025)))
      pop()
    }
  }

  for (var i = 0; i < dimension - 1; i++) {
    for (var j = resolution; j > 0; j--) {
      push()
      translate(windowWidth * 0.5 + (j - Math.floor(resolution * 0.5)) * boardSize * 0.8 * (1 / resolution), windowHeight * 0.5 + (i - Math.floor(dimension * 0.5) + 0.5) * boardSize * maxSize * 1.75 + sin(frame + j * 0.1 + j * 0.1) * boardSize * maxSize * 0.25)
      fill((255 / 63) * abs(Math.floor((frame * resolution + j * 6) % 64)))
      ellipse(0, 0, boardSize * maxSize * abs(sin(frame + j * 0.025)))
      pop()
    }
  }

  frame += deltaTime * 0.001
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
